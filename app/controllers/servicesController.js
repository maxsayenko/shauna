'use strict';

module.exports = function (req, res, next) {
    var viewModel = {
        title: 'Services'
    };

    res.locals = viewModel;
    res.render('services', {
        layout: 'layouts/layout'
    });
};

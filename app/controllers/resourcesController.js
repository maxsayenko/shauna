'use strict';

module.exports = function (req, res, next) {
    var viewModel = {
        title: 'Resources and the Likes'
    };

    res.locals = viewModel;
    res.render('resources', {
        layout: 'layouts/layout'
    });
};

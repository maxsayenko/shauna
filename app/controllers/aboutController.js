'use strict';

module.exports = function (req, res, next) {
    var viewModel = {
        title: 'About The Therapist'
    };

    res.locals = viewModel;
    res.render('about', {
        layout: 'layouts/layout'
    });
};

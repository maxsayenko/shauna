'use strict';

module.exports = function (req, res, next) {
    res.render('home', {
        layout: 'layouts/layout',
        partials: {
            localScripts: "partials/scripts"
        }
    });
};
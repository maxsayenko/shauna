'use strict';

module.exports = function (req, res, next) {
    var viewModel = {
        title: 'Stellar'
    };

    res.locals = viewModel;
    res.render('stellar', {
        layout: 'layouts/layout',
        partials: {
            localScripts: "partials/stellarScripts"
        }
    });
};

$(function () {
    $.stellar();

    $("nav a").click(function (event) {
        var link = $(event.currentTarget);
        var dataTitle = link.data('title');
        console.log($("p[data-title='" + dataTitle + "']"));
        $('html, body').animate({
            scrollTop: $("p[data-title='" + dataTitle + "']").offset().top - $(".menu").height()
        }, 1000);
    });
});
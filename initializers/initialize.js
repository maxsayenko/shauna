var expressConfig = require('./expressConfig');

module.exports = function (app) {

    var controllers = {
        home: require('./../app/controllers/homeController'),
        about: require('./../app/controllers/aboutController'),
        resources: require('./../app/controllers/resourcesController'),
        services: require('./../app/controllers/servicesController'),
        stellar: require('./../app/controllers/stellarController')
    };

    var routes = require('./routeConfig')(controllers);

    // express configurations
    expressConfig(app, routes);
};

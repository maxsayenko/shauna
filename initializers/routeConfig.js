'use strict'

var express = require('express');
var router = express.Router();

module.exports = function (controllers) {
    router.get('/', controllers.home);
    router.get('/about', controllers.about);
    router.get('/resources', controllers.resources);
    router.get('/services', controllers.services);
    router.get('/stellar', controllers.stellar);

    return router;
};
